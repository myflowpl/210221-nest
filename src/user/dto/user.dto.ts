import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';
import { UserModel } from '../models';

export class UserRegisterRequestDto {
  name: string;
  email: string;
  password: string;
}

export class UserRegisterResponseDto {
  user: UserModel;
}

export class UserLoginRequestDto {
  
  @IsEmail()
  @ApiProperty({example: 'piotr@myflow.pl'})
  email: string;

  
  @IsString()
  @MinLength(3)
  @ApiProperty({example: '123'})
  password: string;
}

export class UserLoginResponseDto {
  token: string;
  user: UserModel;
}
