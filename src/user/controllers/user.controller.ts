import { Get, HttpException, HttpStatus, Request, UseGuards, UsePipes, ValidationPipe } from "@nestjs/common";
import { Param } from "@nestjs/common";
import { Controller, Post, Body } from "@nestjs/common";
import { ApiBearerAuth, ApiCreatedResponse, ApiParam } from "@nestjs/swagger";
import { Roles, User } from "../decorators";
import { UserRegisterResponseDto, UserRegisterRequestDto, UserLoginRequestDto, UserLoginResponseDto } from "../dto";
import { AuthGuard } from "../guards";
import { UserModel, UserRole } from "../models";
import { UserByIdPipe } from "../pipes";
import { AuthService, UserService } from "../services";

@Controller('user')
export class UserController {

  constructor(
    private userService: UserService,
    private authService: AuthService,
  ) {}

  @Post('login')
  @UsePipes(new ValidationPipe({ transform: true }))
  async login(@Body() credentials: UserLoginRequestDto): Promise<UserLoginResponseDto> {

    const user = await this.userService.findByCredentials(credentials.email, credentials.password);

    if (!user) {
      throw new HttpException('ValidationError', HttpStatus.UNPROCESSABLE_ENTITY);
    }
    return {
      token: await this.authService.tokenSign({user}),
      user,
    };
  }


  @Post('register')
  // @ApiCreatedResponse({type: UserRegisterResponseDto})

  async register(@Body() data: UserRegisterRequestDto): Promise<UserRegisterResponseDto> {

    const user = await this.userService.create(data);

    // TODO handle errors
    return {
      user,
    };
  }

  @Get()
  @ApiBearerAuth()
  @UseGuards(AuthGuard)
  @Roles(UserRole.ADMIN)
  getUser(@User() user: UserModel) {

    return {user};

  }

  @Get(':id')
  @ApiParam({name: 'id', type: Number})
  getUserById(@Param('id', UserByIdPipe) user: UserModel) {
    return user;
  }
}
