import { ApiProperty } from "@nestjs/swagger";

export class CommentModel {
  id: number;

  @ApiProperty({
    example: 'Piotr',
    description: "Imię osoby komentującej",
    
  })
  name: string;
}
